﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kalkuliatorius
{
    public partial class Form1 : Form
    {
        public string X;
        public string Operacija;
        public Form1()
        {
            InitializeComponent();
        }

        public void AtspausdinkSkaiciu(string skaicius)
        {
            if (Ekranas.Text.Length < 8)
            {
                Ekranas.Text = Ekranas.Text + skaicius;
            }
        }
                      
        private void button10_Click(object sender, EventArgs e)
        {
            var btn = sender as Button;
            AtspausdinkSkaiciu(btn.Text);
        }

        private void buttonC_Click(object sender, EventArgs e)
        {
            Ekranas.Text = "";
        }

        private void buttonKablelis_Click(object sender, EventArgs e)
        {
            if (Ekranas.Text.Contains(""))
            {
                Ekranas.Text = "0,";
            }
            else if (Ekranas.Text.Contains(","))
            {
                Ekranas.Text = Ekranas.Text;
            }
            else
            {
                 Ekranas.Text = Ekranas.Text + ",";
            }
            
        }

        private void buttonLygu_Click(object sender, EventArgs e)
        {
            
            if (Ekranas.Text == "") 
            {
                return;
            }
            
            float x = Convert.ToSingle(X);
            float y = Convert.ToSingle(Ekranas.Text);

            float rezultatas = 0;
            if (Operacija == "+") 
            {
                rezultatas = x + y;
            }
            else if (Operacija == "-")
            {
                rezultatas = x - y;
            }
            else if (Operacija == "*")
            {
                rezultatas = x * y;
            }
            else if (Operacija == "/")
            {
                rezultatas = x / y;
            }

            Ekranas.Text = rezultatas.ToString();
        }
        
        private void buttonPlius_Click(object sender, EventArgs e)
        {
            X = Ekranas.Text;
            Ekranas.Clear();
            Operacija = "+";
        }

        private void buttonMinus_Click(object sender, EventArgs e)
        {
            X = Ekranas.Text;
            Ekranas.Clear();
            Operacija = "-";
        }

        private void buttonDaugyba_Click(object sender, EventArgs e)
        {
            X = Ekranas.Text;
            Ekranas.Clear();
            Operacija = "*";
        }

        private void buttonDalyba_Click(object sender, EventArgs e)
        {
            X = Ekranas.Text;
            Ekranas.Clear();
            Operacija = "/";
        }

        private void buttonSaknis_Click(object sender, EventArgs e)
        {

        }

        private void buttonPliusMinus_Click(object sender, EventArgs e)
        {
            if (Ekranas.Text == "") { return; }
            float y = Convert.ToSingle(Ekranas.Text);
            Ekranas.Text = (y * -1).ToString();
        }
    }
}
