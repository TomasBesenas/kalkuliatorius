﻿namespace Kalkuliatorius
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Ekranas = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.buttonPlius = new System.Windows.Forms.Button();
            this.buttonMinus = new System.Windows.Forms.Button();
            this.buttonDaugyba = new System.Windows.Forms.Button();
            this.buttonDalyba = new System.Windows.Forms.Button();
            this.buttonLygu = new System.Windows.Forms.Button();
            this.buttonC = new System.Windows.Forms.Button();
            this.buttonKablelis = new System.Windows.Forms.Button();
            this.buttonSaknis = new System.Windows.Forms.Button();
            this.buttonPliusMinus = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Ekranas
            // 
            this.Ekranas.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.Ekranas.Location = new System.Drawing.Point(23, 19);
            this.Ekranas.Name = "Ekranas";
            this.Ekranas.Size = new System.Drawing.Size(489, 102);
            this.Ekranas.TabIndex = 0;
            this.Ekranas.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(23, 127);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(93, 79);
            this.button1.TabIndex = 1;
            this.button1.Text = "1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button10_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(122, 127);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(93, 79);
            this.button2.TabIndex = 2;
            this.button2.Text = "2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button10_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(221, 127);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(93, 79);
            this.button3.TabIndex = 3;
            this.button3.Text = "3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button10_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(23, 212);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(93, 79);
            this.button4.TabIndex = 4;
            this.button4.Text = "4";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button10_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(122, 212);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(93, 79);
            this.button5.TabIndex = 5;
            this.button5.Text = "5";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button10_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(221, 212);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(93, 79);
            this.button6.TabIndex = 6;
            this.button6.Text = "6";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button10_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(23, 297);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(93, 79);
            this.button7.TabIndex = 7;
            this.button7.Text = "7";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button10_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(122, 297);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(93, 79);
            this.button8.TabIndex = 8;
            this.button8.Text = "8";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button10_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(221, 297);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(93, 79);
            this.button9.TabIndex = 9;
            this.button9.Text = "9";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button10_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(23, 382);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(192, 79);
            this.button10.TabIndex = 10;
            this.button10.Text = "0";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // buttonPlius
            // 
            this.buttonPlius.Location = new System.Drawing.Point(320, 127);
            this.buttonPlius.Name = "buttonPlius";
            this.buttonPlius.Size = new System.Drawing.Size(93, 79);
            this.buttonPlius.TabIndex = 11;
            this.buttonPlius.Text = "+";
            this.buttonPlius.UseVisualStyleBackColor = true;
            this.buttonPlius.Click += new System.EventHandler(this.buttonPlius_Click);
            // 
            // buttonMinus
            // 
            this.buttonMinus.Location = new System.Drawing.Point(320, 212);
            this.buttonMinus.Name = "buttonMinus";
            this.buttonMinus.Size = new System.Drawing.Size(93, 79);
            this.buttonMinus.TabIndex = 12;
            this.buttonMinus.Text = "-";
            this.buttonMinus.UseVisualStyleBackColor = true;
            this.buttonMinus.Click += new System.EventHandler(this.buttonMinus_Click);
            // 
            // buttonDaugyba
            // 
            this.buttonDaugyba.Location = new System.Drawing.Point(320, 297);
            this.buttonDaugyba.Name = "buttonDaugyba";
            this.buttonDaugyba.Size = new System.Drawing.Size(93, 79);
            this.buttonDaugyba.TabIndex = 13;
            this.buttonDaugyba.Text = "*";
            this.buttonDaugyba.UseVisualStyleBackColor = true;
            this.buttonDaugyba.Click += new System.EventHandler(this.buttonDaugyba_Click);
            // 
            // buttonDalyba
            // 
            this.buttonDalyba.Location = new System.Drawing.Point(320, 382);
            this.buttonDalyba.Name = "buttonDalyba";
            this.buttonDalyba.Size = new System.Drawing.Size(93, 79);
            this.buttonDalyba.TabIndex = 14;
            this.buttonDalyba.Text = "/";
            this.buttonDalyba.UseVisualStyleBackColor = true;
            this.buttonDalyba.Click += new System.EventHandler(this.buttonDalyba_Click);
            // 
            // buttonLygu
            // 
            this.buttonLygu.Location = new System.Drawing.Point(23, 467);
            this.buttonLygu.Name = "buttonLygu";
            this.buttonLygu.Size = new System.Drawing.Size(192, 79);
            this.buttonLygu.TabIndex = 15;
            this.buttonLygu.Text = "=";
            this.buttonLygu.UseVisualStyleBackColor = true;
            this.buttonLygu.Click += new System.EventHandler(this.buttonLygu_Click);
            // 
            // buttonC
            // 
            this.buttonC.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.buttonC.Location = new System.Drawing.Point(221, 467);
            this.buttonC.Name = "buttonC";
            this.buttonC.Size = new System.Drawing.Size(192, 79);
            this.buttonC.TabIndex = 16;
            this.buttonC.Text = "C";
            this.buttonC.UseVisualStyleBackColor = false;
            this.buttonC.Click += new System.EventHandler(this.buttonC_Click);
            // 
            // buttonKablelis
            // 
            this.buttonKablelis.Location = new System.Drawing.Point(221, 382);
            this.buttonKablelis.Name = "buttonKablelis";
            this.buttonKablelis.Size = new System.Drawing.Size(93, 79);
            this.buttonKablelis.TabIndex = 17;
            this.buttonKablelis.Text = ",";
            this.buttonKablelis.UseVisualStyleBackColor = true;
            this.buttonKablelis.Click += new System.EventHandler(this.buttonKablelis_Click);
            // 
            // buttonSaknis
            // 
            this.buttonSaknis.Location = new System.Drawing.Point(419, 127);
            this.buttonSaknis.Name = "buttonSaknis";
            this.buttonSaknis.Size = new System.Drawing.Size(93, 79);
            this.buttonSaknis.TabIndex = 18;
            this.buttonSaknis.Text = "√";
            this.buttonSaknis.UseVisualStyleBackColor = true;
            this.buttonSaknis.Click += new System.EventHandler(this.buttonSaknis_Click);
            // 
            // buttonPliusMinus
            // 
            this.buttonPliusMinus.Location = new System.Drawing.Point(419, 212);
            this.buttonPliusMinus.Name = "buttonPliusMinus";
            this.buttonPliusMinus.Size = new System.Drawing.Size(93, 79);
            this.buttonPliusMinus.TabIndex = 19;
            this.buttonPliusMinus.Text = "+-";
            this.buttonPliusMinus.UseVisualStyleBackColor = true;
            this.buttonPliusMinus.Click += new System.EventHandler(this.buttonPliusMinus_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1387, 889);
            this.Controls.Add(this.buttonPliusMinus);
            this.Controls.Add(this.buttonSaknis);
            this.Controls.Add(this.buttonKablelis);
            this.Controls.Add(this.buttonC);
            this.Controls.Add(this.buttonLygu);
            this.Controls.Add(this.buttonDalyba);
            this.Controls.Add(this.buttonDaugyba);
            this.Controls.Add(this.buttonMinus);
            this.Controls.Add(this.buttonPlius);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Ekranas);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Ekranas;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button buttonPlius;
        private System.Windows.Forms.Button buttonMinus;
        private System.Windows.Forms.Button buttonDaugyba;
        private System.Windows.Forms.Button buttonDalyba;
        private System.Windows.Forms.Button buttonLygu;
        private System.Windows.Forms.Button buttonC;
        private System.Windows.Forms.Button buttonKablelis;
        private System.Windows.Forms.Button buttonSaknis;
        private System.Windows.Forms.Button buttonPliusMinus;
    }
}

